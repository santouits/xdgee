#ifndef SLOW_STRINGS_H
#define SLOW_STRINGS_H


/* Returns NULL or the string, that you have to use free() after you don't need it anymore */
char *slow_string(const char *one, ...);


#endif

