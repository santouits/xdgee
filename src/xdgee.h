#ifndef XDGEE_H
#define XDGEE_H

/* You call it in your program before any other function.
 * It just reads the environment and sets the variables for
 * faster lookup */
int xdgee_init();

/* Creates a directory in the user's defined place, if it doesn't exist, with 0700 mode as by the standard.
 * Returns the full path of the directory that you can use to create inside your files.
 * You have to free the path after use with the free function.
 * If you pass an empty string it returns the root dir path */ 
char *xdgee_open_data_dir(const char *dir_name);
char *xdgee_open_config_dir(const char *dir_name);

/* Get an array of paths that point to an existing data file in the system */
char **xdgee_get_data_file(const char *filename);

/* Get an array of paths that point to an existing config file in the system */
char **xdgee_get_config_file(const char *filename);

/* Free the array you got with xdgee_get_data_file or xdgee_get_config_file */
void xdgee_free_array(char **array);

/* You usually use these functions like this
 
	char **paths = xdgee_get_data_file("some_file");
	if (paths == NULL) {
		// No file was found with that filename
		return;
	}
	char **it = paths;
	while (*it != NULL) {
		char *path = *it++;
		// Open and read the contents of the file with your code
	}
	xdgee_free_array(paths);

*/

const char *xdgee_get_cache_dir();
const char *xdgee_get_runtime_dir();

#endif
