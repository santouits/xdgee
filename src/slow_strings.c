#include "slow_strings.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* We free the old_string if we error */
static char *_slow_string(char *old_string, const char *append) {
	char *ret = old_string;

	size_t old_len = strlen(ret);
	size_t append_len = strlen(append);

	if (append_len == 0) {
		return ret;
	}

	ret = calloc(1, sizeof(char) * (old_len + append_len + 2)); 
	if (ret == NULL) {
		free(old_string);
		return NULL;
	}

	int size = snprintf(ret, old_len + append_len + 2, "%s%s", old_string, append);
	free(old_string);

	return ret;
}

char *slow_string(const char *one, ...) {
	if (one == NULL) {
		return NULL;
	}

	char *ret = calloc(1, sizeof(char));
	if (ret == NULL) {
		return NULL;
	}
	ret[0] = '\0';

	ret = _slow_string(ret, one);
	if (ret == NULL) {
		return NULL;
	}

	va_list ap;
	va_start(ap, one);
	char *arg;
	while ((arg = va_arg(ap, char*)) != NULL) {
		ret = _slow_string(ret, arg);
		if (ret == NULL) {
			va_end(ap);
			return NULL;
		}
	}
	va_end(ap);
	
	return ret;
}
