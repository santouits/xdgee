#include "xdgee.h"

#include "slow_strings.h"

#include <dirent.h>
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

static char *USER_HOME = NULL;
static char *XDG_DATA_HOME = NULL;
static char **XDG_DATA_DIRS = NULL;
static char *XDG_CONFIG_HOME = NULL;
static char **XDG_CONFIG_DIRS = NULL;
static char *XDG_CACHE_HOME = NULL;
static char *XDG_RUNTIME_DIR = NULL;

static int init_home() {
	USER_HOME = getenv("HOME");
	if (USER_HOME == NULL) {
		fprintf(stderr, "HOME is not set by the environment\n");
		return -1;
	}
	printf("$HOME: %s\n", USER_HOME);

	return 0;
}

static bool path_exists(const char *path) {
	if (access(path, F_OK ) == -1 ) {
		return false;
	}

	return true;
}

static char *create_dir(char *path) {
	DIR* dir = opendir(path);
	if (dir) {
		closedir(dir);
		return path;
	} else if (ENOENT == errno) {
		int ret = mkdir(path, 0700);
		if (ret == 0) {
			return path;
		}
	} else if (ENOTDIR == errno) {
		return NULL;
	}
	return NULL;
}

char *xdgee_open_data_dir(const char *dir_name) {
	if (XDG_DATA_HOME == NULL) {
		return NULL;
	}

	char *path = slow_string(XDG_DATA_HOME, "/", dir_name, NULL);
	return create_dir(path);
}

char *xdgee_open_config_dir(const char *dir_name) {
	if (XDG_CONFIG_HOME == NULL) {
		return NULL;
	}
	char *path = slow_string(XDG_CONFIG_HOME, "/", dir_name, NULL);
	return create_dir(path);
}


void xdgee_free_array(char **array) {
	char **it = array;
	while (*it != NULL) {
		free(*it);
		++it;
	}
	free(array);
}

static char **_get_file(char **paths, const char *filename) {
	if (paths == NULL) {
		return NULL;
	}
	char **it = paths;
	char **new_array = malloc(sizeof(char*));
	int size = 1;
	while (*it != NULL) {
		char *path = slow_string(*it, "/", filename, NULL);
		bool exists = path_exists(path);
		if (!exists) {
			free(path);
			++it;
			continue;
		}
		new_array[size - 1] = path;
		char **tmp = realloc(new_array, ++size * sizeof(char*));
		if (tmp == NULL) {
			free(path);
			xdgee_free_array(new_array);
			return NULL;
		}
		new_array = tmp;

		++it;
	}
	new_array[size - 1] = NULL;
	return new_array;
}

char **xdgee_get_data_file(const char *filename) {
	return _get_file(XDG_DATA_DIRS, filename);
}

char **xdgee_get_config_file(const char *filename) {
	return _get_file(XDG_CONFIG_DIRS, filename);
}

static int init_data_home() {
	XDG_DATA_HOME = getenv("XDG_DATA_HOME");
	if (XDG_DATA_HOME == NULL) {
		if (USER_HOME == NULL) {
			fprintf(stderr, "XDG_DATA_HOME and HOME are not set by the environment. Can't get default XDG_DATA_HOME\n");
			return -1;
		}
		XDG_DATA_HOME = slow_string(USER_HOME, "/.local/share", NULL);
	}
	printf("$XDG_DATA_HOME: %s\n", XDG_DATA_HOME);

	return 0;
}

static int init_data_dirs() {
	char *XDG_DATA_DIRS_STRING = getenv("XDG_DATA_DIRS");
	if (XDG_DATA_DIRS_STRING == NULL) {
		XDG_DATA_DIRS_STRING = "/usr/local/share:/usr/share";
	}

	XDG_DATA_DIRS_STRING = slow_string(XDG_DATA_HOME, ":", XDG_DATA_DIRS_STRING, NULL);
	XDG_DATA_DIRS = malloc(sizeof(char*));
	if (XDG_DATA_DIRS == NULL) {
		free(XDG_DATA_DIRS_STRING);
		return -1;
	}
	
	char *next = strtok(XDG_DATA_DIRS_STRING, ":");
	int index = 0;
	while (next != NULL) {
		char **tmp = realloc(XDG_DATA_DIRS, (index + 2) * sizeof(char*));
		if (tmp == NULL) {
			free(XDG_DATA_DIRS);
			XDG_DATA_DIRS = NULL;
			free(XDG_DATA_DIRS_STRING);
			return -1;
		}
		XDG_DATA_DIRS = tmp;
		XDG_DATA_DIRS[index] = strdup(next);
		++index;
		next = strtok(NULL, ":");
	}
	XDG_DATA_DIRS[index] = NULL;

}

static int init_config_home() {
	XDG_CONFIG_HOME = getenv("XDG_CONFIG_HOME");
	if (XDG_CONFIG_HOME == NULL) {
		if (USER_HOME == NULL) {
			fprintf(stderr, "XDG_CONFIG_HOME and HOME are not set by the environment. Can't get default XDG_CONFIG_HOME\n");
			return -1;
		}
		XDG_CONFIG_HOME = slow_string(USER_HOME, "/.config", NULL);
	}
	printf("$XDG_CONFIG_HOME: %s\n", XDG_CONFIG_HOME);

	return 0;
}

static int init_config_dirs() {
	char *XDG_CONFIG_DIRS_STRING = getenv("XDG_CONFIG_DIRS");
	if (XDG_CONFIG_DIRS == NULL) {
		XDG_CONFIG_DIRS_STRING = "/etc/xdg";
	}
	XDG_CONFIG_DIRS_STRING = slow_string(XDG_CONFIG_HOME, ":", XDG_CONFIG_DIRS_STRING, NULL);

	XDG_CONFIG_DIRS = malloc(sizeof(char*));
	if (XDG_CONFIG_DIRS == NULL) {
		return -1;
	}
	
	char *next = strtok(XDG_CONFIG_DIRS_STRING, ":");
	int index = 0;
	while (next != NULL) {
		char **tmp = realloc(XDG_CONFIG_DIRS, (index + 2) * sizeof(char*));
		if (tmp == NULL) {
			free(XDG_CONFIG_DIRS);
			XDG_CONFIG_DIRS = NULL;
			free(XDG_CONFIG_DIRS_STRING);
			return -1;
		}
		XDG_CONFIG_DIRS = tmp;
		XDG_CONFIG_DIRS[index] = strdup(next);
		++index;
		next = strtok(NULL, ":");
	}
	XDG_CONFIG_DIRS[index] = NULL;

	return 0;
}

static int init_cache_home() {
	XDG_CACHE_HOME = getenv("XDG_CACHE_HOME");
	if (XDG_CACHE_HOME == NULL) {
		if (USER_HOME == NULL) {
			fprintf(stderr, "XDG_CONFIG_HOME and HOME are not set by the environment. Can't get default XDG_CONFIG_HOME\n");
			return -1;
		}
		XDG_CACHE_HOME = slow_string(USER_HOME, "/.cache", NULL);
	}
	printf("$XDG_CACHE_HOME: %s\n", XDG_CACHE_HOME);

	return 0;
}

static int init_runtime_dir() {
	XDG_RUNTIME_DIR = getenv("XDG_RUNTIME_DIR");
	if (XDG_RUNTIME_DIR == NULL) {
		// TODO maybe set it to /tmp
		return -1;
	}
	printf("$XDG_RUNTIME_DIR: %s\n", XDG_RUNTIME_DIR);

	return 0;
}
int xdgee_init() {
	init_home();
	init_data_home();
	init_data_dirs();
	init_config_home();
	init_config_dirs();
	init_cache_home();
	init_runtime_dir();
	
	return 0;
};

