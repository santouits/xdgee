#include "src/slow_strings.h"
#include "src/xdgee.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

	char *string = "Bernie Sanders";

	char *path = slow_string(string, "ho ho ho", ":", "f", "", " end", NULL, "afsdfsd");

	if (path != NULL) {
		printf("%s\n", path);
	}

	free(path);

	xdgee_init();

	char **subdirs = xdgee_get_data_file("aclocal");
	char **it = subdirs;
	while (*it != NULL) {
		printf("%s\n", *it++);
	}
	xdgee_free_array(subdirs);

	char *p = xdgee_open_data_dir("");
	if (p != NULL) {
		printf("%s\n", p);
		free(p);
	}

	return 0;
}
